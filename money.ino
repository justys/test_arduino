

//-------NASTAVENÍ---------
#define coin_amount 12    // počet minci
float coin_value[coin_amount] = {1.0, 2.0, 5.0, 10.0, 20.0, 50.0, 1.0, 2.0, 5.0, 10.0, 20.0, 50.0};  
String currency = "CZK"; 
int stb_time = 10000;    // čas pasivity
//-------NASTAVENÍ---------

int coin_signal[coin_amount];    // zde uložena hodnota signálu pro každou velikost mince
int coin_quantity[coin_amount];  // počet minci
byte empty_signal;               // úroveň prázdného signálu
unsigned long standby_timer, reset_timer; // timer
float summ_money = 0;            // součet minci

//-------KNIHOVNY---------
#include "LowPower.h"
#include "EEPROMex.h"
#include "LCD_1602_RUS.h"
//-------KNIHOVNY---------

LCD_1602_RUS lcd(0x27, 16, 2);            

boolean sleep_flag = true;   
//-------TLAČITKA---------
#define button 2         // tlačitko -  wake up 
#define calibr_button 3  // kalibrovat
#define LEDpin 11        // napájení LED diody
#define IRpin 17         // napájení fototranzistoru 
#define IRsens 14        // signál fototranzistoru
//-------TLAČITKA---------
int sens_signal, last_sens_signal;
boolean coin_flag = false;

void setup() {
  Serial.begin(9600);                   
  delay(500);


  pinMode(button, INPUT_PULLUP);
  pinMode(calibr_button, INPUT_PULLUP);

  pinMode(LEDpin, OUTPUT);
  pinMode(IRpin, OUTPUT);

  digitalWrite(LEDpin, 1);
  digitalWrite(IRpin, 1);

//Nastaveni preruseni
  attachInterrupt(0, wake_up, CHANGE);

//START kalibrace
  empty_signal = analogRead(IRsens);  // prazdný signal
  // inicializace displeju
  lcd.init();
  lcd.backlight();

  if (!digitalRead(calibr_button)) {  // jestli zapnute tlačitko kalibrovat
	lcd.clear();
	lcd.setCursor(3, 0);
	lcd.print(L"Servis");
	delay(100);
	reset_timer = millis();
	while (1) {                                   
	  if (millis() - reset_timer > 3000) {        
		// vycistit pocet minci
		for (byte i = 0; i < coin_amount; i++) {
		  coin_quantity[i] = 0;
		  EEPROM.writeInt(20 + i * 2, 0);
		}
		lcd.clear();
		lcd.setCursor(0, 0);
		lcd.print(L"Memory is clear");
		delay(100);
	  }
	  if (digitalRead(calibr_button)) {   
		lcd.clear();
		lcd.setCursor(0, 0);
		lcd.print(L"Calibration");
		break;
	  }
	}
	while (1) {
	  for (byte i = 0; i < coin_amount; i++) {
		lcd.setCursor(0, 1); lcd.print("        ");
		lcd.setCursor(0, 1); lcd.print(coin_value[i]);  // zobrazit cenu mince, kteru kalibruješ
		lcd.setCursor(13, 1); lcd.print(currency);      // zobrazit valuty
		last_sens_signal = empty_signal;
		while (1) {
		   sens_signal = analogRead(IRsens);                                   // načist sensor
		  if (sens_signal > last_sens_signal) last_sens_signal = sens_signal;  // 
		  if (sens_signal - empty_signal > 3) coin_flag = true;                // 
		  if (coin_flag && (abs(sens_signal - empty_signal)) < 2) {            // jestli mince přesně odletěla
			coin_signal[i] = last_sens_signal;                                 // zapsat maximální hodnotu do paměti
			EEPROM.writeInt(i * 2, coin_signal[i]);
			coin_flag = false;
			break;
		  }
		}
	  }
	  break;
	}
  }

  
  for (byte i = 0; i < coin_amount; i++) {
	coin_signal[i] = EEPROM.readInt(i * 2);
	coin_quantity[i] = EEPROM.readInt(20 + i * 2);
	summ_money += coin_quantity[i] * coin_value[i];  
  }

  
	// pro nastavění poslat signaly na port
	for (byte i = 0; i < coin_amount; i++) {
	  Serial.print(coin_value[i]);
	  Serial.print(": ");
	  Serial.println(coin_signal[i]);
	}
//END kalibrace
  
  standby_timer = millis();  // timer do snu
}

void loop() {
  if (sleep_flag) {  
	lcd.init();
	lcd.clear();
	lcd.setCursor(0, 0); lcd.print(L"Na auto");
	lcd.setCursor(0, 1); lcd.print(summ_money);
	lcd.setCursor(13, 1); lcd.print(currency);
	empty_signal = analogRead(IRsens);
	sleep_flag = false;
  }

//START Mince
  last_sens_signal = empty_signal;
  while (1) {
	sens_signal = analogRead(IRsens);  
	if (sens_signal > last_sens_signal) last_sens_signal = sens_signal;
	if (sens_signal - empty_signal > 3) coin_flag = true;
	if (coin_flag && (abs(sens_signal - empty_signal)) < 2) {
	  int delta = 1000; 
	  int min_i = 0;   
	  for (byte i = 0; i < coin_amount; i++) {
	  if (delta > abs(last_sens_signal - coin_signal[i]) ) {
		  delta = abs(last_sens_signal - coin_signal[i]) ;
		  min_i = i;
		}
	  }

	  summ_money += coin_value[min_i];  // к сумме тупо прибавляем цену монетки (дада, сумма считается двумя разными способами. При старте системы суммой всех монет, а тут прибавление
	  lcd.setCursor(0, 1); lcd.print(summ_money);
	  coin_quantity[min_i]++;
	  
	  coin_flag = false;
	  standby_timer = millis();  
	  break;
	}

	
	if (millis() - standby_timer > stb_time) {
	  good_night();
	  break;
	}

	
	while (!digitalRead(button)) {
	  if (millis() - standby_timer > 2000) {
		lcd.clear();

		
		for (byte i = 0; i < coin_amount; i++) {
		  lcd.setCursor(i * 3, 0); lcd.print((int)coin_value[i]);
		  lcd.setCursor(i * 3, 1); lcd.print(coin_quantity[i]);
		}
	  }
	}
  }
  //END Mince
}

// funkce spání
void good_night() {
  // zapsát do EEPROM noví data počtu mimci
  for (byte i = 0; i < coin_amount; i++) {
	EEPROM.updateInt(20 + i * 2, coin_quantity[i]);
  }
  sleep_flag = true;
  // vypnout všechno
 // digitalWrite(disp_power, 0);
  digitalWrite(LEDpin, 0);
  digitalWrite(IRpin, 0);
  delay(100);
  // spát
  LowPower.powerDown(SLEEP_FOREVER, ADC_OFF, BOD_OFF);
}


void wake_up() {
  
	digitalWrite(LEDpin, 1);
  digitalWrite(IRpin, 1);
  standby_timer = millis();  
}
