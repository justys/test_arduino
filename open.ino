#include <Keypad.h>
#include <Servo.h>
#include <LCD_1602_RUS.h>
#define BEEPpin 12        // napájení BEEP diody

Servo myservo;
int pos = 0;

LCD_1602_RUS lcd(0x27, 16, 2);


char pressedKey[5] = {'0','0','0','0'};
// náš kód pro otevření dveří
char code[5] = {'2','3','5','8'};



const byte radky = 4;
const byte sloupce = 3;




char keys[radky][sloupce] = {
  {'1','2','3'},
  {'4','5','6'},
  {'7','8','9'},
  {'*','0','#'}
};

byte pinyRadku[radky] = {10, 9, 8, 7};
byte pinySloupcu[sloupce] = {6, 5, 4};

Keypad klavesnice = Keypad( makeKeymap(keys), pinyRadku, pinySloupcu, radky, sloupce);

void setup() {

  Serial.begin(9600);
  myservo.attach(13);
  pos=200;
  myservo.write(pos);
  lcd.init();
  lcd.backlight();
  lcd.clear();
  pinMode(BEEPpin, OUTPUT);
}

void beep(int delka){
  digitalWrite(BEEPpin, 1);
  delay(delka);
  digitalWrite(BEEPpin, 0);
}

void loop(){

  char customKey = klavesnice.getKey();
  //START Klavesnice a servo nutno dat do while(1)
  if (customKey){
	Serial.print("Key:");
	Serial.println(customKey);
	beep(100);
	// když je stisknuto potvrzovací tlačítko #
	if (customKey == '#')
	{
	beep(200);
	  if ((pressedKey[0] == code[0]) && (pressedKey[1] == code[1]) && (pressedKey[2] == code[2]) && (pressedKey[3] == code[3]))
		{if (pos==200){
		  lcd.init();
		  lcd.backlight();
		  lcd.print(L"Opening");
		  pos=100;
		  myservo.write(pos);
		  pressedKey[0] = '0';
		  pressedKey[1] = '0';
		  pressedKey[2] = '0';
		  pressedKey[3] = '0';
		  delay(1000);
		  lcd.clear();
		  lcd.print(L"Door opened");

		  delay(3000);
		  lcd.clear();
		  lcd.print(L"Press * to close");
		}

		else{

		lcd.init();
		  lcd.backlight();
		  lcd.print(L"Already opened");
		  delay(1000);
		  lcd.clear();
		  lcd.print(L"Press * to close");

		  }


		}

	else
	 {
	  lcd.init();
	  lcd.backlight();
	 lcd.print(L"Wrong Code");
	 pressedKey[0] = '0';
	 pressedKey[1] = '0';
	 pressedKey[2] = '0';
	 pressedKey[3] = '0';
	 delay(1000);
	lcd.clear();
	 }
	}

	// když není stisknuto potvrzovací tlačítko #
	 else
	{


	pressedKey[0] = pressedKey[1];
	pressedKey[1] = pressedKey[2];
	pressedKey[2] = pressedKey[3];
	pressedKey[3] = customKey;



	}}
	if (customKey == '*'){

	if (pos == 100)
		 {
		  lcd.init();
		  lcd.backlight();
		  lcd.print(L"Closing");

		  pos=200;
		  myservo.write(pos);
		  delay(1000);
		  lcd.clear();
		  lcd.print(L"Door closed");
		  pressedKey[0] = '0';
		  pressedKey[1] = '0';
		  pressedKey[2] = '0';
		  pressedKey[3] = '0';
		  delay(3000);
		  lcd.clear();




} else
	 {
	  lcd.init();
	  lcd.backlight();
	 lcd.print(L"Already closed");
	 pressedKey[0] = '0';
	 pressedKey[1] = '0';
	 pressedKey[2] = '0';
	 pressedKey[3] = '0';
	 delay(3000);
	 lcd.clear();

	 }

	}
	//END Klavesnice a servo
}
