/*========================================INCLUDE========================================*/
#include <Keypad.h>
#include <Servo.h>
#include "LCD_1602_RUS.h"
#include "LowPower.h"
#include "EEPROMex.h"


/*=========================================DEFINE========================================*/
// pins
#define BEEP_PIN 12
#define button 2          // tlačitko -  wake up 
#define calibr_button 3   // kalibrovat
#define LEDpin 11         // napájení LED diody
#define IRpin 17          // napájení fototranzistoru 
#define IRsens 14         // signál fototranzistoru
#define coin_amount 12    // počet minci


/*========================================CONSTANTS======================================*/
const byte ROWS = 4;
const byte COLS = 3;


/*========================================VARIABLES======================================*/
int door_position = 200;
char pressed_key[5] = {'0', '0', '0', '0', '0'};
char code[5] = {'2', '3', '5', '8'};
byte rows_pins[ROWS] = {10, 9, 8, 7};
byte cols_pins[COLS] = {6, 5, 4};
char keys[ROWS][COLS] = {
    {'1', '2', '3'},
    {'4', '5', '6'},
    {'7', '8', '9'},
    {'*', '0', '#'}
};

Servo my_servo;
LCD_1602_RUS lcd(0x27, 16, 2);
Keypad my_keypad = Keypad(makeKeymap(keys), rows_pins, cols_pins, ROWS, COLS);

float coin_value[coin_amount] = {1.0, 2.0, 5.0, 10.0, 20.0, 50.0, 1.0, 2.0, 5.0, 10.0, 20.0, 50.0};
String currency = "CZK";
int stb_time = 10000;    // čas pasivity

int coin_signal[coin_amount];    // zde uložena hodnota signálu pro každou velikost mince
int coin_quantity[coin_amount];  // počet minci
byte empty_signal;               // úroveň prázdného signálu
unsigned long standby_timer, reset_timer; // timer
float summ_money = 0;            // součet minci

boolean sleep_flag = true;

int sens_signal, last_sens_signal;
boolean coin_flag = false;


/*==========================================SETUP========================================*/
void setup() {
    // serial init
    Serial.begin(9600);
    delay(500);

    // keypad init
    my_keypad.addEventListener(keypad_event);

    // servo init
    my_servo.attach(13);
    my_servo.write(door_position);

    // display init
    lcd.init();
    lcd.backlight();
    delay(1000);
    lcd.init();
    lcd.print(L"Ready");
    lcd.clear();

    // beep init
    pinMode(BEEP_PIN, OUTPUT);

    delay(500);

    pinMode(button, INPUT_PULLUP);
    pinMode(calibr_button, INPUT_PULLUP);

    pinMode(LEDpin, OUTPUT);
    pinMode(IRpin, OUTPUT);

    digitalWrite(LEDpin, 1);
    digitalWrite(IRpin, 1);

//Nastaveni preruseni
    attachInterrupt(0, wake_up, CHANGE);

//START kalibrace
    empty_signal = analogRead(IRsens);  // prazdný signal
    // inicializace displeju
    lcd.init();
    lcd.backlight();

    if (!digitalRead(calibr_button)) {  // jestli zapnute tlačitko kalibrovat
        lcd.clear();
        lcd.setCursor(3, 0);
        lcd.print(L"Servis");
        delay(100);
        reset_timer = millis();
        while (1) {
            if (millis() - reset_timer > 3000) {
                // vycistit pocet minci
                for (byte i = 0; i < coin_amount; i++) {
                    coin_quantity[i] = 0;
                    EEPROM.writeInt(20 + i * 2, 0);
                }
                lcd.clear();
                lcd.setCursor(0, 0);
                lcd.print(L"Memory is clear");
                delay(100);
            }
            if (digitalRead(calibr_button)) {
                lcd.clear();
                lcd.setCursor(0, 0);
                lcd.print(L"Calibration");
                break;
            }
        }
        while (1) {
            for (byte i = 0; i < coin_amount; i++) {
                lcd.setCursor(0, 1);
                lcd.print("        ");
                lcd.setCursor(0, 1);
                lcd.print(coin_value[i]);  // zobrazit cenu mince, kteru kalibruješ
                lcd.setCursor(13, 1);
                lcd.print(currency);      // zobrazit valuty
                last_sens_signal = empty_signal;
                while (1) {
                    sens_signal = analogRead(IRsens);                                   // načist sensor
                    if (sens_signal > last_sens_signal) last_sens_signal = sens_signal;  //
                    if (sens_signal - empty_signal > 3) coin_flag = true;                //
                    if (coin_flag && (abs(sens_signal - empty_signal)) < 2) {            // jestli mince přesně odletěla
                        coin_signal[i] = last_sens_signal;                                 // zapsat maximální hodnotu do paměti
                        EEPROM.writeInt(i * 2, coin_signal[i]);
                        coin_flag = false;
                        break;
                    }
                }
            }
            break;
        }
    }


    for (byte i = 0; i < coin_amount; i++) {
        coin_signal[i] = EEPROM.readInt(i * 2);
        coin_quantity[i] = EEPROM.readInt(20 + i * 2);
        summ_money += coin_quantity[i] * coin_value[i];
    }


    // pro nastavění poslat signaly na port
    for (byte i = 0; i < coin_amount; i++) {
        Serial.print(coin_value[i]);
        Serial.print(": ");
        Serial.println(coin_signal[i]);
    }
//END kalibrace

    standby_timer = millis();  // timer do snu

}


/*==========================================LOOP=========================================*/
void loop() {
    char custom_key = my_keypad.getKey();

    // door control
    if (custom_key == '#') {
        Serial.println("Door control");
        door_control();
    }

    // calibration
    if (custom_key == '1') {
        Serial.println("Calibration");
    }

    if (custom_key == '2') {
        Serial.println("Coin counter");
    }

    if (sleep_flag) {
        lcd.init();
        lcd.clear();
        lcd.setCursor(0, 0);
        lcd.print(L"Na auto");
        lcd.setCursor(0, 1);
        lcd.print(summ_money);
        lcd.setCursor(13, 1);
        lcd.print(currency);
        empty_signal = analogRead(IRsens);
        sleep_flag = false;
    }

//START Mince
    last_sens_signal = empty_signal;
    while (1) {
        sens_signal = analogRead(IRsens);
        if (sens_signal > last_sens_signal) last_sens_signal = sens_signal;
        if (sens_signal - empty_signal > 3) coin_flag = true;
        if (coin_flag && (abs(sens_signal - empty_signal)) < 2) {
            int delta = 1000;
            int min_i = 0;
            for (byte i = 0; i < coin_amount; i++) {
                if (delta > abs(last_sens_signal - coin_signal[i]) ) {
                    delta = abs(last_sens_signal - coin_signal[i]) ;
                    min_i = i;
                }
            }

            summ_money += coin_value[min_i];  // к сумме тупо прибавляем цену монетки (дада, сумма считается двумя разными способами. При старте системы суммой всех монет, а тут прибавление
            lcd.setCursor(0, 1);
            lcd.print(summ_money);
            coin_quantity[min_i]++;

            coin_flag = false;
            standby_timer = millis();
            break;
        }


        if (millis() - standby_timer > stb_time) {
            good_night();
            break;
        }


        while (!digitalRead(button)) {
            if (millis() - standby_timer > 2000) {
                lcd.clear();


                for (byte i = 0; i < coin_amount; i++) {
                    lcd.setCursor(i * 3, 0);
                    lcd.print((int)coin_value[i]);
                    lcd.setCursor(i * 3, 1);
                    lcd.print(coin_quantity[i]);
                }
            }
        }
    }
    //END Mince
}


/*========================================FUNCTIONS======================================*/
void keypad_event(KeypadEvent key) {
    switch (my_keypad.getState()) {
    case PRESSED:
        if (key == '*') {
            close_the_door();
        }
        break;

    case RELEASED:
        break;

    case HOLD:
        break;
    }
}


void beep(int length) {
    digitalWrite(BEEP_PIN, 1);
    delay(length);
    digitalWrite(BEEP_PIN, 0);
}


void clear_pressed_key() {
    for (int i = 0; i < 5; i++) {
        pressed_key[i] = 0;
    }
}

void log_pressed_key(char pressed_key) {
    Serial.print("Pressed key: ");
    Serial.println(pressed_key);
}


void close_the_door() {
    if (door_position == 100) {
        lcd.backlight();
        lcd.print(L"Closing");
        door_position = 200;
        my_servo.write(door_position);
        delay(1000);
        lcd.clear();
        lcd.print(L"Door closed");
        clear_pressed_key();
        delay(3000);
        lcd.clear();
    } else {
        lcd.backlight();
        lcd.clear();
        lcd.print(L"Already closed");
        clear_pressed_key();
        delay(3000);
        lcd.clear();
    }
}


void door_control() {
    beep(200);
    int counter = 0;
    while(counter != 4) {
        char custom_key = my_keypad.getKey();
        if (custom_key) {
            beep(100);
            log_pressed_key(custom_key);
            if (custom_key != '*') {
                lcd.print("*");
                pressed_key[counter] = custom_key;
                counter++;
            } else {
                break;
            }
        }
    }

    boolean error = false;
    for (int i = 0; i < 4; i++) {
        if (pressed_key[i] != code[i]) {
            error = true;
        }
    }

    if (!error) {
        // open the door
        if (door_position == 200) {
            lcd.backlight();
            lcd.clear();
            lcd.print(L"Opening");
            door_position = 100;
            my_servo.write(door_position);
            clear_pressed_key();
            delay(1000);
            lcd.clear();
            lcd.print(L"Door Opened");
            delay(3000);
        } else {
            lcd.backlight();
            lcd.clear();
            lcd.print(L"Already opened");
            delay(1000);
        }

        lcd.clear();
        lcd.print(L"Press * to close");
    } else {
        // wrong password
        lcd.backlight();
        lcd.clear();
        lcd.print(L"Wrong Code");
        clear_pressed_key();
        delay(1000);
        lcd.clear();
    }
}

void good_night() {
    // zapsát do EEPROM noví data počtu mimci
    for (byte i = 0; i < coin_amount; i++) {
        EEPROM.updateInt(20 + i * 2, coin_quantity[i]);
    }
    sleep_flag = true;
    // vypnout všechno
// digitalWrite(disp_power, 0);
    digitalWrite(LEDpin, 0);
    digitalWrite(IRpin, 0);
    delay(100);
    // spát
    LowPower.powerDown(SLEEP_FOREVER, ADC_OFF, BOD_OFF);
}

void wake_up() {

    digitalWrite(LEDpin, 1);
    digitalWrite(IRpin, 1);
    standby_timer = millis();
}